﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task27.Models;

namespace Task27.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetASupervisor()
        {
            return _context.Supervisors;
        }
        [HttpPost]
        public ActionResult<Supervisor> ConsumeSupervisor(Supervisor supervisor)
        {
            _context.Supervisors.Add(supervisor);
            _context.SaveChanges();

            return CreatedAtAction("GetSpecificSupervisorID", new Supervisor { Id = supervisor.Id }, supervisor);
        }
        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSpecificSupervisorID(int id)
        {
            var supervisor = _context.Supervisors.Find(id);

            return supervisor;
        }
        [HttpPut("{Id}")]
        public ActionResult UpdateSupervisor(int id, Supervisor supervisor)
        {
            if (id!= supervisor.Id)
            {
                return BadRequest();
            }

            _context.Entry(supervisor).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        [HttpDelete("{id}")]
        public ActionResult<Supervisor> DeleteSupervisor(int id)
        {
            var supervisor = _context.Supervisors.Find(id);

            if (supervisor == null)
            {
                return NotFound();
            }

            _context.Supervisors.Remove(supervisor);
            _context.SaveChanges();

            return supervisor;
        }

    }
}